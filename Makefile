run:
	mvn spring-boot:run

create-database:
	mvn liquibase:update

install:
	mvn clean install

test:
	mvn clean test

package:
	mvn clean package

rollback-database:
	mvn liquibase:rollback -Dliquibase.rollbackCount=1

