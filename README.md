# Almoxarifado

## Instalação

# Criando o banco de dados
```bash
make create-database 
```

# Rollback do banco de dados
```bash
make rollback-database 
```

# Rodando os testes unitários
```bash
make test 
```
# Instalando a aplicação
```bash
make package 
```

# Rodando a aplicação
```bash
make run 
```
