package br.com.memory.almoxarifado.controller.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.memory.almoxarifado.service.repository.UnidadeMedidaService;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@WebMvcTest(UnidadeMedidaController.class)
class UnidadeMedidaControllerTest {

	@MockBean
	UnidadeMedidaService unidadeMedidaService;

	@Autowired
	MockMvc mockMvc;

	@Test
	void istoDeveSalvarUnidadeMedida() throws JsonProcessingException, Exception {

		var unidadeMedida = AlmoxarifadoUtils.buildUnidadeMedida();
		when(unidadeMedidaService.save(any())).thenReturn(unidadeMedida);

		String unidadeMedidaAsJson = AlmoxarifadoUtils.asJson(unidadeMedida);

		assertEquals(unidadeMedidaAsJson, mockMvc
				.perform(MockMvcRequestBuilders.post("/unidade-medida").content(unidadeMedidaAsJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString());
	}
}
