package br.com.memory.almoxarifado.controller.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.memory.almoxarifado.service.repository.LocalDestinoService;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@WebMvcTest(LocalDestinoController.class)
class LocalDestinoControllerTest {

	@MockBean
	LocalDestinoService localDestinoService;

	@Autowired
	MockMvc mockMvc;

	@Test
	void istoDeveSalvarLocalDestino() throws JsonProcessingException, Exception {

		var localDestino = AlmoxarifadoUtils.buildLocalDestino();
		when(localDestinoService.save(any())).thenReturn(localDestino);

		String localDestinoAsJson = AlmoxarifadoUtils.asJson(localDestino);

		assertEquals(localDestinoAsJson, mockMvc
				.perform(MockMvcRequestBuilders.post("/local-destino").content(localDestinoAsJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString());
	}
}
