package br.com.memory.almoxarifado.controller.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.memory.almoxarifado.service.repository.ProdutoService;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@WebMvcTest(ProdutoController.class)
class ProdutoControllerTest {

	@MockBean
	ProdutoService produtoService;

	@Autowired
	MockMvc mockMvc;

	@Test
	void istoDeveSalvarProduto() throws JsonProcessingException, Exception {

		var produto = AlmoxarifadoUtils.buildProduto();
		when(produtoService.save(any())).thenReturn(produto);

		String produtoAsJson = AlmoxarifadoUtils.asJson(produto);

		assertEquals(produtoAsJson, mockMvc
				.perform(MockMvcRequestBuilders.post("/produto").content(produtoAsJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString());
	}
}
