package br.com.memory.almoxarifado.controller.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.memory.almoxarifado.service.repository.AlmoxarifadoService;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@WebMvcTest(AlmoxarifadoController.class)
class AlmoxarifadoControllerTest {

	@MockBean
	AlmoxarifadoService almoxarifadoService;

	@Autowired
	MockMvc mockMvc;

	@Test
	void istoDeveSalvarAlmoxarifado() throws JsonProcessingException, Exception {

		var almoxarifado = AlmoxarifadoUtils.buildAlmoxarifado();
		when(almoxarifadoService.save(any())).thenReturn(almoxarifado);

		String almoxarifadoAsJson = AlmoxarifadoUtils.asJson(almoxarifado);

		assertEquals(almoxarifadoAsJson, mockMvc
				.perform(MockMvcRequestBuilders.post("/almoxarifado").content(almoxarifadoAsJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString());
	}
}
