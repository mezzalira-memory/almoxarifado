package br.com.memory.almoxarifado.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.memory.almoxarifado.model.EntradaAlmoxarifado;
import br.com.memory.almoxarifado.model.SaidaAlmoxarifado;
import br.com.memory.almoxarifado.service.MantemAlmoxarifadoService;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@WebMvcTest(MantemAlmoxarifadoController.class)
class MantemAlmoxarifadoControllerTest {

	@MockBean
	MantemAlmoxarifadoService mantemAlmoxarifadoService;

	@Autowired
	MockMvc mockMvc;

	@Test
	void istoDeveGerarEntradaNoEstoque() throws JsonProcessingException, Exception {

		var itemEstoque = AlmoxarifadoUtils.buildItemEstoque();
		when(mantemAlmoxarifadoService.atualizarAlmoxarifado(any(EntradaAlmoxarifado.class))).thenReturn(itemEstoque);

		String itemEstoqueAsJson = AlmoxarifadoUtils.asJson(itemEstoque);

		assertEquals(itemEstoqueAsJson, mockMvc
				.perform(MockMvcRequestBuilders.post("/entrada-almoxarifado").content(itemEstoqueAsJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString());
	}

	@Test
	void istoDeveGerarSaidaDoEstoque() throws JsonProcessingException, Exception {

		var itemEstoque = AlmoxarifadoUtils.buildItemEstoque();
		when(mantemAlmoxarifadoService.atualizarAlmoxarifado(any(SaidaAlmoxarifado.class))).thenReturn(itemEstoque);

		String itemEstoqueAsJson = AlmoxarifadoUtils.asJson(itemEstoque);

		assertEquals(itemEstoqueAsJson, mockMvc
				.perform(MockMvcRequestBuilders.post("/saida-almoxarifado").content(itemEstoqueAsJson)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString());
	}

	@Test
	void istoDeveRetornarItensEstoquePorAlmoxarifado() throws JsonProcessingException, Exception {

		var itemsEstoque = Collections.singletonList(AlmoxarifadoUtils.buildItemEstoque());
		when(mantemAlmoxarifadoService.verEstoque(AlmoxarifadoUtils.MAGIC_NUMBER)).thenReturn(itemsEstoque);

		assertEquals(new ObjectMapper().writeValueAsString(itemsEstoque),
				mockMvc.perform(MockMvcRequestBuilders.get("/ver-estoque/" + AlmoxarifadoUtils.MAGIC_NUMBER)
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn()
						.getResponse().getContentAsString());
	}

}
