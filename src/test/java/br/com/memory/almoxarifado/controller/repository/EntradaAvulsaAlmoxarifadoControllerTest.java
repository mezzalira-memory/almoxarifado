package br.com.memory.almoxarifado.controller.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;

import br.com.memory.almoxarifado.service.repository.EntradaAvulsaAlmoxarifadoService;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@WebMvcTest(EntradaAvulsaAlmoxarifadoController.class)
class EntradaAvulsaAlmoxarifadoControllerTest {

	@MockBean
	EntradaAvulsaAlmoxarifadoService entradaAvulsaAlmoxarifadoService;

	@Autowired
	MockMvc mockMvc;

	@Test
	void istoDeveSalvarEntradaAvulsaAlmoxarifado() throws JsonProcessingException, Exception {

		var entradaAvulsaAlmoxarifado = AlmoxarifadoUtils.buildEntradaAvulsaAlmoxarifado();
		when(entradaAvulsaAlmoxarifadoService.save(any())).thenReturn(entradaAvulsaAlmoxarifado);

		String entradaAvulsaAlmoxarifadoAsJson = AlmoxarifadoUtils.asJson(entradaAvulsaAlmoxarifado);

		assertEquals(entradaAvulsaAlmoxarifadoAsJson,
				mockMvc.perform(MockMvcRequestBuilders.post("/entrada-avulsa-almoxarifado")
						.content(entradaAvulsaAlmoxarifadoAsJson).contentType(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isCreated())
						.andReturn().getResponse().getContentAsString());
	}
}
