package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.SaidaAlmoxarifado;
import br.com.memory.almoxarifado.repository.SaidaAlmoxarifadoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class SaidaAlmoxarifadoServiceTest {

	@Mock
	SaidaAlmoxarifadoRepository saidaAlmoxarifadoRepository;

	@InjectMocks
	SaidaAlmoxarifadoService saidaAlmoxarifadoService;

	@Test
	void istoDeveSalvarSaidaAlmoxarifado() {
		var saidaAlmoxarifado = AlmoxarifadoUtils.buildSaidaAlmoxarifado();
		when(saidaAlmoxarifadoRepository.save(any(SaidaAlmoxarifado.class))).thenReturn(saidaAlmoxarifado);
		assertNotNull(saidaAlmoxarifadoService.save(saidaAlmoxarifado));
	}
}
