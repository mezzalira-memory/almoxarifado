package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.EntradaAvulsaAlmoxarifado;
import br.com.memory.almoxarifado.repository.EntradaAvulsaAlmoxarifadoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class EntradaAvulsaAlmoxarifadoServiceTest {

	@Mock
	EntradaAvulsaAlmoxarifadoRepository entradaAvulsaAlmoxarifadoRepository;

	@InjectMocks
	EntradaAvulsaAlmoxarifadoService entradaAvulsaAlmoxarifadoService;

	@Test
	void istoDeveSalvarEntradaAvulsaAlmoxarifado() {
		var entradaAvulsaAlmoxarifado = AlmoxarifadoUtils.buildEntradaAvulsaAlmoxarifado();
		when(entradaAvulsaAlmoxarifadoRepository.save(any(EntradaAvulsaAlmoxarifado.class)))
				.thenReturn(entradaAvulsaAlmoxarifado);
		assertNotNull(entradaAvulsaAlmoxarifadoService.save(entradaAvulsaAlmoxarifado));
	}

}
