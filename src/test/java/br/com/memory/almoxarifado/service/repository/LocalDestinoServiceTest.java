package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.LocalDestino;
import br.com.memory.almoxarifado.repository.LocalDestinoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class LocalDestinoServiceTest {

	@Mock
	LocalDestinoRepository localDestinoRepository;

	@InjectMocks
	LocalDestinoService localDestinoService;

	@Test
	void istoDeveSalvarLocalDestino() {
		var localDestino = AlmoxarifadoUtils.buildLocalDestino();
		when(localDestinoRepository.save(any(LocalDestino.class))).thenReturn(localDestino);
		assertNotNull(localDestinoService.save(localDestino));
	}
}
