package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.Almoxarifado;
import br.com.memory.almoxarifado.repository.AlmoxarifadoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class AlmoxarifadoServiceTest {

	@Mock
	AlmoxarifadoRepository almoxarifadoRepository;

	@InjectMocks
	AlmoxarifadoService almoxarifadoService;

	@Test
	void istoDeveSalvarAlmoxarifado() {
		var almoxarifado = AlmoxarifadoUtils.buildAlmoxarifado();
		when(almoxarifadoRepository.save(any(Almoxarifado.class))).thenReturn(almoxarifado);
		assertNotNull(almoxarifadoService.save(almoxarifado).getEstoque());
	}

	@Test
	void istoDeveRetornarAlmoxarifado() {
		var optionalAlmoxarifado = Optional.of(AlmoxarifadoUtils.buildAlmoxarifado());
		when(almoxarifadoRepository.findById(anyLong())).thenReturn(optionalAlmoxarifado);
		assertNotNull(almoxarifadoService.findById(AlmoxarifadoUtils.MAGIC_NUMBER).get());
	}
}
