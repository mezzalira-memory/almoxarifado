package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.Produto;
import br.com.memory.almoxarifado.repository.ProdutoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class ProdutoServiceTest {

	@Mock
	ProdutoRepository produtoRepository;

	@InjectMocks
	ProdutoService produtoService;

	@Test
	void istoDeveSalvarProduto() {
		var produto = AlmoxarifadoUtils.buildProduto();
		when(produtoRepository.save(any(Produto.class))).thenReturn(produto);
		assertNotNull(produtoService.save(produto));
	}
}
