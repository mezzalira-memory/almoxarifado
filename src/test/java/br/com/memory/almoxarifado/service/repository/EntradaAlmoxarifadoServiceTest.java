package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.EntradaAlmoxarifado;
import br.com.memory.almoxarifado.repository.EntradaAlmoxarifadoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class EntradaAlmoxarifadoServiceTest {

	@Mock
	EntradaAlmoxarifadoRepository entradaAlmoxarifadoRepository;

	@InjectMocks
	EntradaAlmoxarifadoService entradaAlmoxarifadoService;

	@Test
	void istoDeveSalvarEntradaAlmoxarifado() {
		var entradaAlmoxarifado = AlmoxarifadoUtils.buildEntradaAlmoxarifado();
		when(entradaAlmoxarifadoRepository.save(any(EntradaAlmoxarifado.class))).thenReturn(entradaAlmoxarifado);
		assertNotNull(entradaAlmoxarifadoService.save(entradaAlmoxarifado));
	}
}
