package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.EntradaNafAlmoxarifado;
import br.com.memory.almoxarifado.repository.EntradaNafAlmoxarifadoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class EntradaNafAlmoxarifadoServiceTest {

	@Mock
	EntradaNafAlmoxarifadoRepository entradaNafAlmoxarifadoRepository;

	@InjectMocks
	EntradaNafAlmoxarifadoService entradaNafAlmoxarifadoService;

	@Test
	void istoDeveSalvarEntradaNafAlmoxarifado() {
		var entradaNafAlmoxarifado = AlmoxarifadoUtils.buildEntradaNafAlmoxarifado();
		when(entradaNafAlmoxarifadoRepository.save(any(EntradaNafAlmoxarifado.class))).thenReturn(entradaNafAlmoxarifado);
		assertNotNull(entradaNafAlmoxarifadoService.save(entradaNafAlmoxarifado));
	}

}
