package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.UnidadeMedida;
import br.com.memory.almoxarifado.repository.UnidadeMedidaRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class UnidadeMedidaServiceTest {

	@Mock
	UnidadeMedidaRepository unidadeMedidaRepository;

	@InjectMocks
	UnidadeMedidaService unidadeMedidaService;

	@Test
	void istoDeveSalvarProduto() {
		var unidadeMedida = AlmoxarifadoUtils.buildUnidadeMedida();
		when(unidadeMedidaRepository.save(any(UnidadeMedida.class))).thenReturn(unidadeMedida);
		assertNotNull(unidadeMedidaService.save(unidadeMedida));
	}
}
