package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.EntradaDoacaoAlmoxarifado;
import br.com.memory.almoxarifado.repository.EntradaDoacaoAlmoxarifadoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class EntradaDoacaoAlmoxarifadoServiceTest {

	@Mock
	EntradaDoacaoAlmoxarifadoRepository entradaDoacaoAlmoxarifadoRepository;

	@InjectMocks
	EntradaDoacaoAlmoxarifadoService entradaDoacaoAlmoxarifadoService;

	@Test
	void istoDeveSalvarEntradaDoacaoAlmoxarifado() {
		var entradaDoacaoAlmoxarifado = AlmoxarifadoUtils.buildEntradaDoacaoAlmoxarifado();
		when(entradaDoacaoAlmoxarifadoRepository.save(any(EntradaDoacaoAlmoxarifado.class)))
				.thenReturn(entradaDoacaoAlmoxarifado);
		assertNotNull(entradaDoacaoAlmoxarifadoService.save(entradaDoacaoAlmoxarifado));
	}

}
