package br.com.memory.almoxarifado.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.EntradaAlmoxarifado;
import br.com.memory.almoxarifado.model.Estoque;
import br.com.memory.almoxarifado.model.ItemEstoque;
import br.com.memory.almoxarifado.model.Produto;
import br.com.memory.almoxarifado.model.SaidaAlmoxarifado;
import br.com.memory.almoxarifado.service.repository.AlmoxarifadoService;
import br.com.memory.almoxarifado.service.repository.EntradaAlmoxarifadoService;
import br.com.memory.almoxarifado.service.repository.ItemEstoqueService;
import br.com.memory.almoxarifado.service.repository.SaidaAlmoxarifadoService;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class MantemAlmoxarifadoServiceTest {

	@Mock
	EntradaAlmoxarifadoService entradaAlmoxarifadoService;

	@Mock
	SaidaAlmoxarifadoService saidaAlmoxarifadoService;

	@Mock
	ItemEstoqueService itemEstoqueService;

	@Mock
	AlmoxarifadoService almoxarifadoService;

	@InjectMocks
	MantemAlmoxarifadoService mantemAlmoxarifadoService;

	@Test
	void istoDeveRegistrarEntradaAlmoxarifadoQuandoItemEstoqueNaoExiste() {

		var entradaAlmoxarifado = AlmoxarifadoUtils.buildEntradaAlmoxarifado();
		when(entradaAlmoxarifadoService.save(any(EntradaAlmoxarifado.class))).thenReturn(entradaAlmoxarifado);

		when(almoxarifadoService.findById(anyLong())).thenReturn(Optional.of(AlmoxarifadoUtils.buildAlmoxarifado()));

		when(itemEstoqueService.findByProdutoAndEstoque(any(Produto.class), any(Estoque.class))).thenReturn(null);

		when(itemEstoqueService.save(any(ItemEstoque.class))).thenReturn(AlmoxarifadoUtils.buildItemEstoque());

		assertEquals(entradaAlmoxarifado.getQuantidade(),
				mantemAlmoxarifadoService.atualizarAlmoxarifado(entradaAlmoxarifado).getQuantidade());
	}

	@Test
	void istoDeveRegistrarEntradaAlmoxarifadoQuandoItemEstoqueExiste() {

		var entradaAlmoxarifado = AlmoxarifadoUtils.buildEntradaAlmoxarifado();
		when(entradaAlmoxarifadoService.save(any(EntradaAlmoxarifado.class))).thenReturn(entradaAlmoxarifado);

		when(almoxarifadoService.findById(anyLong())).thenReturn(Optional.of(AlmoxarifadoUtils.buildAlmoxarifado()));

		var itemEstoque = AlmoxarifadoUtils.buildItemEstoque();
		when(itemEstoqueService.findByProdutoAndEstoque(any(Produto.class), any(Estoque.class)))
				.thenReturn(itemEstoque);

		when(itemEstoqueService.save(any(ItemEstoque.class))).thenReturn(itemEstoque);

		assertEquals(entradaAlmoxarifado.getQuantidade() * 2,
				mantemAlmoxarifadoService.atualizarAlmoxarifado(entradaAlmoxarifado).getQuantidade());
	}

	@Test
	void istoDeveRegistrarSaidaAlmoxarifado() {

		var saidaAlmoxarifado = AlmoxarifadoUtils.buildSaidaAlmoxarifado();
		when(saidaAlmoxarifadoService.save(any(SaidaAlmoxarifado.class))).thenReturn(saidaAlmoxarifado);

		when(almoxarifadoService.findById(anyLong())).thenReturn(Optional.of(AlmoxarifadoUtils.buildAlmoxarifado()));

		var itemEstoque = AlmoxarifadoUtils.buildItemEstoque();
		when(itemEstoqueService.findByProdutoAndEstoque(any(Produto.class), any(Estoque.class)))
				.thenReturn(itemEstoque);

		when(itemEstoqueService.save(any(ItemEstoque.class))).thenReturn(itemEstoque);

		assertEquals(saidaAlmoxarifado.getQuantidade() - saidaAlmoxarifado.getQuantidade(),
				mantemAlmoxarifadoService.atualizarAlmoxarifado(saidaAlmoxarifado).getQuantidade());
	}

	@Test
	void istoDeveRetornarItensEstoquePorAlmoxarifado() {

		when(almoxarifadoService.findById(anyLong())).thenReturn(Optional.of(AlmoxarifadoUtils.buildAlmoxarifado()));

		when(itemEstoqueService.findByEstoque(any(Estoque.class)))
				.thenReturn(Collections.singletonList(AlmoxarifadoUtils.buildItemEstoque()));

		assertFalse(mantemAlmoxarifadoService.verEstoque(AlmoxarifadoUtils.MAGIC_NUMBER).isEmpty());
	}

}
