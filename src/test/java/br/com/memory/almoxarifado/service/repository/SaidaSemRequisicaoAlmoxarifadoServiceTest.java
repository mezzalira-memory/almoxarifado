package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.SaidaSemRequisicaoAlmoxarifado;
import br.com.memory.almoxarifado.repository.SaidaSemRequisicaoAlmoxarifadoRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class SaidaSemRequisicaoAlmoxarifadoServiceTest {

	@Mock
	SaidaSemRequisicaoAlmoxarifadoRepository saidaSemRequisicaoAlmoxarifadoRepository;

	@InjectMocks
	SaidaSemRequisicaoAlmoxarifadoService saidaSemRequisicaoAlmoxarifadoService;

	@Test
	void istoDeveSalvarSaidaSemRequisicaoAlmoxarifado() {
		var saidaSemRequisicaoAlmoxarifado = AlmoxarifadoUtils.buildSaidaSemRequisicaoAlmoxarifado();
		when(saidaSemRequisicaoAlmoxarifadoRepository.save(any(SaidaSemRequisicaoAlmoxarifado.class)))
				.thenReturn(saidaSemRequisicaoAlmoxarifado);
		assertNotNull(saidaSemRequisicaoAlmoxarifadoService.save(saidaSemRequisicaoAlmoxarifado));
	}

}
