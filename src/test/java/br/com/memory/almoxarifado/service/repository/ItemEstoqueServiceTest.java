package br.com.memory.almoxarifado.service.repository;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.memory.almoxarifado.model.Estoque;
import br.com.memory.almoxarifado.model.ItemEstoque;
import br.com.memory.almoxarifado.model.Produto;
import br.com.memory.almoxarifado.repository.ItemEstoqueRepository;
import br.com.memory.almoxarifado.utils.AlmoxarifadoUtils;

@ExtendWith(MockitoExtension.class)
class ItemEstoqueServiceTest {

	@Mock
	ItemEstoqueRepository itemEstoqueRepository;

	@InjectMocks
	ItemEstoqueService itemEstoqueService;

	@Test
	void istoDeveSalvarItemEstoque() {
		var itemEstoque = AlmoxarifadoUtils.buildItemEstoque();
		when(itemEstoqueRepository.save(any(ItemEstoque.class))).thenReturn(itemEstoque);
		assertNotNull(itemEstoqueService.save(itemEstoque));
	}

	@Test
	void istoDeveRetornarItemEstoquePorProdutoEstoque() {
		var itemEstoque = AlmoxarifadoUtils.buildItemEstoque();
		when(itemEstoqueRepository.findByProdutoAndEstoque(any(Produto.class), any(Estoque.class)))
				.thenReturn(itemEstoque);
		assertNotNull(itemEstoqueService.findByProdutoAndEstoque(itemEstoque.getProduto(), itemEstoque.getEstoque()));
	}

	@Test
	void istoDeveRetornarItemsEstoquePorEstoque() {
		var itemsEstoque = Collections.singletonList(AlmoxarifadoUtils.buildItemEstoque());
		when(itemEstoqueRepository.findByEstoque(any(Estoque.class))).thenReturn(itemsEstoque);
		assertFalse(itemEstoqueService.findByEstoque(AlmoxarifadoUtils.buildEstoque()).isEmpty());
	}
}
