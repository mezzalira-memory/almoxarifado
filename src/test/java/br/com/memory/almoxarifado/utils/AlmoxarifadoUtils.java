package br.com.memory.almoxarifado.utils;

import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import br.com.memory.almoxarifado.model.Almoxarifado;
import br.com.memory.almoxarifado.model.EntradaAlmoxarifado;
import br.com.memory.almoxarifado.model.EntradaAvulsaAlmoxarifado;
import br.com.memory.almoxarifado.model.EntradaDoacaoAlmoxarifado;
import br.com.memory.almoxarifado.model.EntradaNafAlmoxarifado;
import br.com.memory.almoxarifado.model.Estoque;
import br.com.memory.almoxarifado.model.ItemEstoque;
import br.com.memory.almoxarifado.model.LocalDestino;
import br.com.memory.almoxarifado.model.Produto;
import br.com.memory.almoxarifado.model.SaidaAlmoxarifado;
import br.com.memory.almoxarifado.model.SaidaSemRequisicaoAlmoxarifado;
import br.com.memory.almoxarifado.model.UnidadeMedida;

public class AlmoxarifadoUtils {

	public static final Long MAGIC_NUMBER = 1L;
	public static final LocalDate MAGIC_DATE = LocalDate.now();
	private static final String MAGIC_TEXT = "texto";

	public static final <T> String asJson(T object) throws JsonProcessingException {
		return JsonMapper.builder().addModule(new JavaTimeModule()).build().writeValueAsString(object);
	}

	public static EntradaNafAlmoxarifado buildEntradaNafAlmoxarifado() {
		return EntradaNafAlmoxarifado.builder().anoExercicio(MAGIC_NUMBER.intValue()).dataNotaFiscal(MAGIC_DATE)
				.serieNotaFiscal(MAGIC_NUMBER.intValue()).numeroNotaFiscal(MAGIC_NUMBER.intValue()).build();
	}

	public static EntradaDoacaoAlmoxarifado buildEntradaDoacaoAlmoxarifado() {
		return EntradaDoacaoAlmoxarifado.builder().motivo(MAGIC_TEXT).fornecedor(MAGIC_NUMBER.intValue())
				.dataNotaFiscal(MAGIC_DATE).serieNotaFiscal(MAGIC_NUMBER.intValue())
				.numeroNotaFiscal(MAGIC_NUMBER.intValue()).build();
	}

	public static EntradaAvulsaAlmoxarifado buildEntradaAvulsaAlmoxarifado() {
		return EntradaAvulsaAlmoxarifado.builder().motivo(MAGIC_TEXT).build();
	}

	public static EntradaAlmoxarifado buildEntradaAlmoxarifado() {
		return EntradaAlmoxarifado.builder().quantidade(MAGIC_NUMBER.intValue())
				.valorUnitario(MAGIC_NUMBER.doubleValue()).dataValidade(MAGIC_DATE).produto(buildProdutoId())
				.unidadeMedida(buildUnidadeMedidaId()).almoxarifado(buildAlmoxarifadoId()).build();
	}

	public static SaidaSemRequisicaoAlmoxarifado buildSaidaSemRequisicaoAlmoxarifado() {
		return SaidaSemRequisicaoAlmoxarifado.builder().requisitante(MAGIC_NUMBER.intValue())
				.pedido(MAGIC_NUMBER.intValue()).unidadeOrcamentaria(MAGIC_NUMBER.intValue())
				.localDestino(buildLocalDestinoId()).motivo(MAGIC_TEXT).build();
	}

	public static SaidaAlmoxarifado buildSaidaAlmoxarifado() {
		return SaidaAlmoxarifado.builder().unidadeMedida(buildUnidadeMedidaId()).quantidade(MAGIC_NUMBER.intValue())
				.dataValidade(MAGIC_DATE).produto(buildProdutoId()).almoxarifado(buildAlmoxarifadoId()).build();
	}

	public static ItemEstoque buildItemEstoque() {
		return ItemEstoque.builder().quantidade(MAGIC_NUMBER.intValue()).produto(buildProdutoId())
				.estoque(buildEstoqueId()).build();
	}

	public static Almoxarifado buildAlmoxarifado() {
		return Almoxarifado.builder().nome(MAGIC_TEXT).build();
	}

	public static Produto buildProduto() {
		return Produto.builder().nome(MAGIC_TEXT).build();
	}

	public static Estoque buildEstoque() {
		return Estoque.builder().id(MAGIC_NUMBER).build();
	}

	public static UnidadeMedida buildUnidadeMedida() {
		return UnidadeMedida.builder().nome(MAGIC_TEXT).build();
	}

	public static LocalDestino buildLocalDestino() {
		return LocalDestino.builder().nome(MAGIC_TEXT).build();
	}

	private static Produto buildProdutoId() {
		return Produto.builder().id(MAGIC_NUMBER).build();
	}

	private static Almoxarifado buildAlmoxarifadoId() {
		return Almoxarifado.builder().id(MAGIC_NUMBER).build();
	}

	private static Estoque buildEstoqueId() {
		return Estoque.builder().id(MAGIC_NUMBER).build();
	}

	private static UnidadeMedida buildUnidadeMedidaId() {
		return UnidadeMedida.builder().id(MAGIC_NUMBER).build();
	}

	private static LocalDestino buildLocalDestinoId() {
		return LocalDestino.builder().id(MAGIC_NUMBER).build();
	}

}
