package br.com.memory.almoxarifado.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.EntradaAlmoxarifado;
import br.com.memory.almoxarifado.model.ItemEstoque;
import br.com.memory.almoxarifado.model.SaidaAlmoxarifado;
import br.com.memory.almoxarifado.service.MantemAlmoxarifadoService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class MantemAlmoxarifadoController {

	private final MantemAlmoxarifadoService mantemAlmoxarifadoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/entrada-almoxarifado")
	public ItemEstoque save(@RequestBody EntradaAlmoxarifado entradaAlmoxarifado) {
		return mantemAlmoxarifadoService.atualizarAlmoxarifado(entradaAlmoxarifado);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/saida-almoxarifado")
	public ItemEstoque save(@RequestBody SaidaAlmoxarifado saidaAlmoxarifado) {
		return mantemAlmoxarifadoService.atualizarAlmoxarifado(saidaAlmoxarifado);
	}

	@GetMapping
	@RequestMapping("/ver-estoque/{idAlmoxarifado}")
	public List<ItemEstoque> verEstoque(@PathVariable Long idAlmoxarifado) {
		return mantemAlmoxarifadoService.verEstoque(idAlmoxarifado);
	}

}
