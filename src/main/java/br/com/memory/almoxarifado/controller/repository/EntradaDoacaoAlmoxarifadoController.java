package br.com.memory.almoxarifado.controller.repository;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.EntradaDoacaoAlmoxarifado;
import br.com.memory.almoxarifado.service.repository.EntradaDoacaoAlmoxarifadoService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class EntradaDoacaoAlmoxarifadoController {

	private final EntradaDoacaoAlmoxarifadoService entradaDoacaoAlmoxarifadoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/entrada-doacao-almoxarifado")
	public EntradaDoacaoAlmoxarifado save(@RequestBody EntradaDoacaoAlmoxarifado entradaDoacaoAlmoxarifado) {
		return entradaDoacaoAlmoxarifadoService.save(entradaDoacaoAlmoxarifado);
	}
}
