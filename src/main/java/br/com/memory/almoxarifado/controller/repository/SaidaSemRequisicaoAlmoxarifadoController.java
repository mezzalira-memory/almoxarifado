package br.com.memory.almoxarifado.controller.repository;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.SaidaSemRequisicaoAlmoxarifado;
import br.com.memory.almoxarifado.service.repository.SaidaSemRequisicaoAlmoxarifadoService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class SaidaSemRequisicaoAlmoxarifadoController {

	private final SaidaSemRequisicaoAlmoxarifadoService saidaSemRequisicaoAlmoxarifadoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/saida-sem-requisicao-almoxarifado")
	public SaidaSemRequisicaoAlmoxarifado save(@RequestBody SaidaSemRequisicaoAlmoxarifado saidaSemRequisicaoAlmoxarifado) {
		return saidaSemRequisicaoAlmoxarifadoService.save(saidaSemRequisicaoAlmoxarifado);
	}
}
