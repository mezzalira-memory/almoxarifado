package br.com.memory.almoxarifado.controller.repository;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.LocalDestino;
import br.com.memory.almoxarifado.service.repository.LocalDestinoService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class LocalDestinoController {

	private final LocalDestinoService localDestinoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/local-destino")
	public LocalDestino save(@RequestBody LocalDestino localDestino) {
		return localDestinoService.save(localDestino);
	}
}
