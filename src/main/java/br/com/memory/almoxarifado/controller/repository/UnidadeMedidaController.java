package br.com.memory.almoxarifado.controller.repository;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.UnidadeMedida;
import br.com.memory.almoxarifado.service.repository.UnidadeMedidaService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class UnidadeMedidaController {

	private final UnidadeMedidaService unidadeMedidaService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/unidade-medida")
	public UnidadeMedida save(@RequestBody UnidadeMedida unidadeMedida) {
		return unidadeMedidaService.save(unidadeMedida);
	}
}
