package br.com.memory.almoxarifado.controller.repository;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.Produto;
import br.com.memory.almoxarifado.service.repository.ProdutoService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class ProdutoController {

	private final ProdutoService produtoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/produto")
	public Produto save(@RequestBody Produto produto) {
		return produtoService.save(produto);
	}
}
