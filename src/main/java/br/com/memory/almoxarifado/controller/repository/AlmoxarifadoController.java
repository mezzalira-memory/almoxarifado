package br.com.memory.almoxarifado.controller.repository;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.Almoxarifado;
import br.com.memory.almoxarifado.service.repository.AlmoxarifadoService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class AlmoxarifadoController {

	private final AlmoxarifadoService almoxarifadoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/almoxarifado")
	public Almoxarifado save(@RequestBody Almoxarifado almoxarifado) {
		return almoxarifadoService.save(almoxarifado);
	}
}
