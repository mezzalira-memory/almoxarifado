package br.com.memory.almoxarifado.controller.repository;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.EntradaNafAlmoxarifado;
import br.com.memory.almoxarifado.service.repository.EntradaNafAlmoxarifadoService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class EntradaNafAlmoxarifadoController {

	private final EntradaNafAlmoxarifadoService entradaNafAlmoxarifadoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/entrada-naf-almoxarifado")
	public EntradaNafAlmoxarifado save(@RequestBody EntradaNafAlmoxarifado entradaNafAlmoxarifado) {
		return entradaNafAlmoxarifadoService.save(entradaNafAlmoxarifado);
	}
}
