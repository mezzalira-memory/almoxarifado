package br.com.memory.almoxarifado.controller.repository;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.memory.almoxarifado.model.EntradaAvulsaAlmoxarifado;
import br.com.memory.almoxarifado.service.repository.EntradaAvulsaAlmoxarifadoService;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class EntradaAvulsaAlmoxarifadoController {

	private final EntradaAvulsaAlmoxarifadoService entradaAvulsaAlmoxarifadoService;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping("/entrada-avulsa-almoxarifado")
	public EntradaAvulsaAlmoxarifado save(@RequestBody EntradaAvulsaAlmoxarifado entradaAvulsaAlmoxarifado) {
		return entradaAvulsaAlmoxarifadoService.save(entradaAvulsaAlmoxarifado);
	}
}
