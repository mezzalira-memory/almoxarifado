package br.com.memory.almoxarifado.service.repository;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.EntradaAvulsaAlmoxarifado;
import br.com.memory.almoxarifado.repository.EntradaAvulsaAlmoxarifadoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EntradaAvulsaAlmoxarifadoService {

	private final EntradaAvulsaAlmoxarifadoRepository entradaAvulsaAlmoxarifadoRepository;

	public EntradaAvulsaAlmoxarifado save(EntradaAvulsaAlmoxarifado entradaAvulsaAlmoxarifado) {
		return entradaAvulsaAlmoxarifadoRepository.save(entradaAvulsaAlmoxarifado);
	}
}
