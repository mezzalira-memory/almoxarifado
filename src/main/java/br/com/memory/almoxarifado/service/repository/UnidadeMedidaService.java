package br.com.memory.almoxarifado.service.repository;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.UnidadeMedida;
import br.com.memory.almoxarifado.repository.UnidadeMedidaRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UnidadeMedidaService {

	private final UnidadeMedidaRepository unidadeMedidaRepository;

	public UnidadeMedida save(UnidadeMedida unidadeMedida) {
		return unidadeMedidaRepository.save(unidadeMedida);
	}
}
