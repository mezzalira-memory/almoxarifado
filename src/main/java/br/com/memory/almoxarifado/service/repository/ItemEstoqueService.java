package br.com.memory.almoxarifado.service.repository;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.Estoque;
import br.com.memory.almoxarifado.model.ItemEstoque;
import br.com.memory.almoxarifado.model.Produto;
import br.com.memory.almoxarifado.repository.ItemEstoqueRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ItemEstoqueService {

	private final ItemEstoqueRepository itemEstoqueRepository;

	public ItemEstoque save(ItemEstoque itemEstoque) {
		return itemEstoqueRepository.save(itemEstoque);
	}

	public ItemEstoque findByProdutoAndEstoque(Produto produto, Estoque estoque) {
		return itemEstoqueRepository.findByProdutoAndEstoque(produto, estoque);
	}

	public List<ItemEstoque> findByEstoque(Estoque estoque) {
		return itemEstoqueRepository.findByEstoque(estoque);
	}
}
