package br.com.memory.almoxarifado.service.repository;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.SaidaSemRequisicaoAlmoxarifado;
import br.com.memory.almoxarifado.repository.SaidaSemRequisicaoAlmoxarifadoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class SaidaSemRequisicaoAlmoxarifadoService {

	private final SaidaSemRequisicaoAlmoxarifadoRepository saidaSemRequisicaoAlmoxarifadoRepository;

	public SaidaSemRequisicaoAlmoxarifado save(SaidaSemRequisicaoAlmoxarifado saidaSemRequisicaoAlmoxarifado) {
		return saidaSemRequisicaoAlmoxarifadoRepository.save(saidaSemRequisicaoAlmoxarifado);
	}
}
