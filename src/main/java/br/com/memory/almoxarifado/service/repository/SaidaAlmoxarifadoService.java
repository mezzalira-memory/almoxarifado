package br.com.memory.almoxarifado.service.repository;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.SaidaAlmoxarifado;
import br.com.memory.almoxarifado.repository.SaidaAlmoxarifadoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class SaidaAlmoxarifadoService {

	private final SaidaAlmoxarifadoRepository saidaAlmoxarifadoRepository;

	public SaidaAlmoxarifado save(SaidaAlmoxarifado saidaAlmoxarifado) {
		return saidaAlmoxarifadoRepository.save(saidaAlmoxarifado);
	}
}
