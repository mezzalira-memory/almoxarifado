package br.com.memory.almoxarifado.service.repository;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.EntradaNafAlmoxarifado;
import br.com.memory.almoxarifado.repository.EntradaNafAlmoxarifadoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EntradaNafAlmoxarifadoService {

	private final EntradaNafAlmoxarifadoRepository entradaNafAlmoxarifadoRepository;

	public EntradaNafAlmoxarifado save(EntradaNafAlmoxarifado entradaNafAlmoxarifado) {
		return entradaNafAlmoxarifadoRepository.save(entradaNafAlmoxarifado);
	}
}
