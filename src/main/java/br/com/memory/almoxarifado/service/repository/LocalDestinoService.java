package br.com.memory.almoxarifado.service.repository;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.LocalDestino;
import br.com.memory.almoxarifado.repository.LocalDestinoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class LocalDestinoService {

	private final LocalDestinoRepository LocalDestinoRepository;

	public LocalDestino save(LocalDestino localDestino) {
		return LocalDestinoRepository.save(localDestino);
	}
}
