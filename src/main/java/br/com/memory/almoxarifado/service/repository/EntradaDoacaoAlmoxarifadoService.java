package br.com.memory.almoxarifado.service.repository;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.EntradaDoacaoAlmoxarifado;
import br.com.memory.almoxarifado.repository.EntradaDoacaoAlmoxarifadoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EntradaDoacaoAlmoxarifadoService {

	private final EntradaDoacaoAlmoxarifadoRepository entradaDoacaoAlmoxarifadoRepository;

	public EntradaDoacaoAlmoxarifado save(EntradaDoacaoAlmoxarifado entradaDoacaoAlmoxarifado) {
		return entradaDoacaoAlmoxarifadoRepository.save(entradaDoacaoAlmoxarifado);
	}
}
