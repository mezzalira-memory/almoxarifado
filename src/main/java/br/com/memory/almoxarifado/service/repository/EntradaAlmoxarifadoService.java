package br.com.memory.almoxarifado.service.repository;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.EntradaAlmoxarifado;
import br.com.memory.almoxarifado.repository.EntradaAlmoxarifadoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class EntradaAlmoxarifadoService {

	private final EntradaAlmoxarifadoRepository entradaAlmoxarifadoRepository;

	public EntradaAlmoxarifado save(EntradaAlmoxarifado entradaAlmoxarifado) {
		return entradaAlmoxarifadoRepository.save(entradaAlmoxarifado);
	}
}
