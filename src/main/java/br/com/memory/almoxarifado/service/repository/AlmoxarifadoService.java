package br.com.memory.almoxarifado.service.repository;

import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.Almoxarifado;
import br.com.memory.almoxarifado.repository.AlmoxarifadoRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AlmoxarifadoService {

	private final AlmoxarifadoRepository almoxarifadoRepository;
	
	public Almoxarifado save(Almoxarifado almoxarifado) {
		return almoxarifadoRepository.save(almoxarifado);
	}
	
	public Optional<Almoxarifado> findById(Long id) {
		return almoxarifadoRepository.findById(id);
	}
}
