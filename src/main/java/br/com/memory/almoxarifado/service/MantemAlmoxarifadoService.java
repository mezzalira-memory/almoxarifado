package br.com.memory.almoxarifado.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import br.com.memory.almoxarifado.model.EntradaAlmoxarifado;
import br.com.memory.almoxarifado.model.Estoque;
import br.com.memory.almoxarifado.model.ItemEstoque;
import br.com.memory.almoxarifado.model.Produto;
import br.com.memory.almoxarifado.model.SaidaAlmoxarifado;
import br.com.memory.almoxarifado.service.repository.AlmoxarifadoService;
import br.com.memory.almoxarifado.service.repository.EntradaAlmoxarifadoService;
import br.com.memory.almoxarifado.service.repository.ItemEstoqueService;
import br.com.memory.almoxarifado.service.repository.SaidaAlmoxarifadoService;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MantemAlmoxarifadoService {

	private final EntradaAlmoxarifadoService entradaAlmoxarifadoService;

	private final SaidaAlmoxarifadoService saidaAlmoxarifadoService;

	private final ItemEstoqueService itemEstoqueService;

	private final AlmoxarifadoService almoxarifadoService;

	@Transactional
	public ItemEstoque atualizarAlmoxarifado(EntradaAlmoxarifado entradaAlmoxarifado) {

		entradaAlmoxarifadoService.save(entradaAlmoxarifado);

		return atualizarAlmoxarifado(entradaAlmoxarifado.getAlmoxarifado().getId(), entradaAlmoxarifado.getProduto(),
				entradaAlmoxarifado.getQuantidade());
	}

	@Transactional
	public ItemEstoque atualizarAlmoxarifado(SaidaAlmoxarifado saidaAlmoxarifado) {

		saidaAlmoxarifadoService.save(saidaAlmoxarifado);

		return atualizarAlmoxarifado(saidaAlmoxarifado.getAlmoxarifado().getId(), saidaAlmoxarifado.getProduto(),
				-saidaAlmoxarifado.getQuantidade());
	}

	private ItemEstoque atualizarAlmoxarifado(Long idAlmoxarifado, Produto produto, Integer quantidade) {
		return almoxarifadoService.findById(idAlmoxarifado)
				.map(almoxarifado -> atualizarEstoque(almoxarifado.getEstoque(), produto, quantidade)).orElse(null);
	}

	private ItemEstoque atualizarEstoque(Estoque estoque, Produto produto, Integer quantidade) {

		var itemEstoque = Optional.ofNullable(itemEstoqueService.findByProdutoAndEstoque(produto, estoque))
				.orElseGet(ItemEstoque::new);

		itemEstoque.setProduto(produto);
		itemEstoque.setEstoque(estoque);
		itemEstoque.setQuantidade(itemEstoque.getQuantidade() + quantidade);

		return itemEstoqueService.save(itemEstoque);
	}

	public List<ItemEstoque> verEstoque(Long idAlmoxarifado) {
		return almoxarifadoService.findById(idAlmoxarifado)
				.map(almoxarifado -> itemEstoqueService.findByEstoque(almoxarifado.getEstoque()))
				.orElse(Collections.emptyList());
	}

}
