package br.com.memory.almoxarifado.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@Entity
public class EntradaAlmoxarifado {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Integer quantidade;
	
	@ManyToOne
	private Produto produto;
	
	@ManyToOne
	private Almoxarifado almoxarifado;

	@ManyToOne
	private UnidadeMedida unidadeMedida;

	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING)
	private LocalDate dataValidade;

	private Double valorUnitario;
	
	@ManyToOne
	private EntradaNafAlmoxarifado entradaNafAlmoxarifado;

	@ManyToOne
	private EntradaDoacaoAlmoxarifado entradaDoacaoAlmoxarifado;

	@ManyToOne
	private EntradaAvulsaAlmoxarifado entradaAvulsaAlmoxarifado;

}
