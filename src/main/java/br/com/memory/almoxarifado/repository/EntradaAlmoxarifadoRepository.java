package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.EntradaAlmoxarifado;

public interface EntradaAlmoxarifadoRepository extends JpaRepository<EntradaAlmoxarifado, Long> {

}
