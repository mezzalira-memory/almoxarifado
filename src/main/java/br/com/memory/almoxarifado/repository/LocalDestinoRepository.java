package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.LocalDestino;

public interface LocalDestinoRepository extends JpaRepository<LocalDestino, Long> {

}
