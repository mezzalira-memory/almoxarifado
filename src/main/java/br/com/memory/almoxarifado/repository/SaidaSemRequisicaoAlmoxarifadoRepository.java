package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.SaidaSemRequisicaoAlmoxarifado;

public interface SaidaSemRequisicaoAlmoxarifadoRepository extends JpaRepository<SaidaSemRequisicaoAlmoxarifado, Long> {

}
