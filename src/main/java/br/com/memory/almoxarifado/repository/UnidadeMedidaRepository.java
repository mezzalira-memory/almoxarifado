package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.UnidadeMedida;

public interface UnidadeMedidaRepository extends JpaRepository<UnidadeMedida, Long> {

}
