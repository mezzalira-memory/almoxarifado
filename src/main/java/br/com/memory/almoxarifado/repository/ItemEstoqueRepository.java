package br.com.memory.almoxarifado.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.Estoque;
import br.com.memory.almoxarifado.model.ItemEstoque;
import br.com.memory.almoxarifado.model.Produto;

public interface ItemEstoqueRepository extends JpaRepository<ItemEstoque, Long> {

	ItemEstoque findByProdutoAndEstoque(Produto produto, Estoque estoque);

	List<ItemEstoque> findByEstoque(Estoque estoque);

}
