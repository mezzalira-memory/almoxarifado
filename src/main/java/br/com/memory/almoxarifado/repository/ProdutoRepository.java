package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {

}
