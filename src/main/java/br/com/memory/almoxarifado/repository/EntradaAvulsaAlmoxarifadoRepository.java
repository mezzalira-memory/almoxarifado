package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.EntradaAvulsaAlmoxarifado;

public interface EntradaAvulsaAlmoxarifadoRepository extends JpaRepository<EntradaAvulsaAlmoxarifado, Long> {

}
