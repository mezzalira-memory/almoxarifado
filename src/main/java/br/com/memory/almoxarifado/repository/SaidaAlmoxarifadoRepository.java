package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.SaidaAlmoxarifado;

public interface SaidaAlmoxarifadoRepository extends JpaRepository<SaidaAlmoxarifado, Long> {

}
