package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.EntradaNafAlmoxarifado;

public interface EntradaNafAlmoxarifadoRepository extends JpaRepository<EntradaNafAlmoxarifado, Long> {

}
