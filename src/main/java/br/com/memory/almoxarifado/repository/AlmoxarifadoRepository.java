package br.com.memory.almoxarifado.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.memory.almoxarifado.model.Almoxarifado;

public interface AlmoxarifadoRepository extends JpaRepository<Almoxarifado, Long> {

}
