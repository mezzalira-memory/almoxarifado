CREATE TABLE produto(
   id serial PRIMARY KEY,
   nome VARCHAR (255) UNIQUE NOT NULL
);

CREATE TABLE estoque(
   id serial PRIMARY KEY
);

CREATE TABLE item_estoque(
   id serial PRIMARY KEY,
   estoque_id INT NOT NULL, 
   produto_id INT NOT NULL, 
   quantidade INT NOT NULL, 
   FOREIGN KEY(estoque_id) REFERENCES estoque(id),
   FOREIGN KEY(produto_id) REFERENCES produto(id)
);

CREATE TABLE almoxarifado(
   id serial PRIMARY KEY,
   nome VARCHAR (255) UNIQUE NOT NULL,
   estoque_id INT UNIQUE NOT NULL, 
   FOREIGN KEY(estoque_id) REFERENCES estoque(id) ON DELETE CASCADE
);

CREATE TABLE unidade_medida(
   id serial PRIMARY KEY,
   nome VARCHAR (255) UNIQUE NOT NULL
);

CREATE TABLE entrada_naf_almoxarifado(
   id serial PRIMARY KEY,
   ano_exercicio INT NOT NULL,
   numero_nota_fiscal INT NOT NULL,
   serie_nota_fiscal INT NOT NULL,
   data_nota_fiscal DATE NOT NULL
);

CREATE TABLE entrada_doacao_almoxarifado(
   id serial PRIMARY KEY,
   fornecedor INT NOT NULL,
   numero_nota_fiscal INT NOT NULL,
   serie_nota_fiscal INT NOT NULL,
   data_nota_fiscal DATE NOT NULL,
   motivo VARCHAR (255) NOT NULL
);

CREATE TABLE entrada_avulsa_almoxarifado(
   id serial PRIMARY KEY,
   motivo VARCHAR (255) NOT NULL
);

CREATE TABLE entrada_almoxarifado(
   id serial PRIMARY KEY,
   almoxarifado_id INT NOT NULL, 
   produto_id INT NOT NULL, 
   quantidade INT NOT NULL, 
   unidade_medida_id INT NOT NULL, 
   data_validade DATE NOT NULL, 
   valor_unitario NUMERIC(10,2) NOT NULL, 
   entrada_naf_almoxarifado_id INT, 
   entrada_doacao_almoxarifado_id INT, 
   entrada_avulsa_almoxarifado_id INT, 
   FOREIGN KEY(almoxarifado_id) REFERENCES almoxarifado(id),
   FOREIGN KEY(produto_id) REFERENCES produto(id),
   FOREIGN KEY(unidade_medida_id) REFERENCES unidade_medida(id),
   FOREIGN KEY(entrada_naf_almoxarifado_id) REFERENCES entrada_naf_almoxarifado(id),
   FOREIGN KEY(entrada_doacao_almoxarifado_id) REFERENCES entrada_doacao_almoxarifado(id),
   FOREIGN KEY(entrada_avulsa_almoxarifado_id) REFERENCES entrada_avulsa_almoxarifado(id)
);

CREATE TABLE local_destino(
   id serial PRIMARY KEY,
   nome VARCHAR (255) NOT NULL
);

CREATE TABLE saida_sem_requisicao_almoxarifado(
   id serial PRIMARY KEY,
   pedido INT, 
   requisitante INT NOT NULL, 
   unidade_orcamentaria INT NOT NULL, 
   motivo VARCHAR (255) NOT NULL,
   local_destino_id INT, 
   FOREIGN KEY(local_destino_id) REFERENCES local_destino(id)
);

CREATE TABLE saida_almoxarifado(
   id serial PRIMARY KEY,
   almoxarifado_id INT NOT NULL, 
   produto_id INT NOT NULL, 
   quantidade INT NOT NULL, 
   unidade_medida_id INT NOT NULL, 
   data_validade DATE NOT NULL, 
   saida_sem_requisicao_almoxarifado_id INT, 
   FOREIGN KEY(almoxarifado_id) REFERENCES almoxarifado(id),
   FOREIGN KEY(produto_id) REFERENCES produto(id),
   FOREIGN KEY(unidade_medida_id) REFERENCES unidade_medida(id),
   FOREIGN KEY(saida_sem_requisicao_almoxarifado_id) REFERENCES saida_sem_requisicao_almoxarifado(id)
);
